# Flyin

Elements fly in to the page when page scroll reaches them.

| Option       | Default | Type     | Description                                     |
|--------------|---------|----------|-------------------------------------------------|
| from         | left    | String   | Direction the element will fly in from          |
| scrollOffset | 50      | Number   | wait this many extra pixels to fly in           |
| flyout       | false   | Boolean  | have the elements fly out                       |
| duration     | 500     | Number   | animation duration                              |
| delay        | 0       | Number   | ms to wait for fly in                           |
| easing       | linear  | String   | Easing effect (load jquery-ui for more options) |
| afterFlyin   | Null    | Function | Run a function after the fly in                 |
| minHeight    | 0       | Number   | minimum height the window needs to be           |
| minWidth     | null    | Number   | minimum width the window needs to be            |


####HTML

You may specify some options in HTML to control the behavior of specific
flyin elements individually.


| HTML Option             | JS Equivalent |
|-------------------------|---------------|
| data-flyin-scrollOffset | scrollOffset  |
| data-flyin-from         | from          |
| data-flyin-flyout       | flyout        |
| data-flyin-duration     | duration      |
| data-flyin-delay        | delay         |
| data-flyin-easing       | easing        |
| data-flyin-minHeight    | minHeight     |
| data-flyin-minWidth     | minWidth      |

```html
<div class="service-quicklinks flyin">
	<div class="flyin-item" data-flyin-from="left">
		<!-- Content -->
	</div>
	<div class="flyin-item" data-flyin-from="right">
		<!-- Content -->
	</div>
</div>
```

####CSS

You will want to hide overflow on the parent of the flyin-item. This isn't necessarily
the .flyin element, but may be further up the DOM tree.

```css
.flyin {
 	position: relative;
 	overflow: hidden;  
}
.flyin .flyin-item {
	position: relative;
}
```

####Javascript

Initialize script

```javascript
<script src="/public/scripts/jquery.flyin.min.js"></script>

<!-- Optional (only if you need a fancier animation)
<script src="/public/scripts/jquery-ui.min.js"></script> -->

<script>
$(document).ready(function(){
	$('.flyin').flyin({
		scrollOffset: 100,
		flyout: true,
	});
});
</script>
```
